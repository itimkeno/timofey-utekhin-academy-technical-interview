﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class Rover {
  public static void CalculateRoverPath(string[,] map) {

    if (map.Length == 0) {
      ExceptWriteToFile("Wrong input: map is empty.");
      throw new Exception("Wrong input: map is empty.");
    }

    if (map[0, 0] == "X") {
      ExceptWriteToFile("Cannot start a movement because the first square is marked with an X.");
      throw new Exception("Cannot start a movement because the first square is marked with an X.");
    }


    List<Node> graph = CreateGraph(map);

    List<Node> reachables = new() { graph.First() };
    List<Node> explored = new();

    Node nodeWithBestPath = new(0, 0, 0);
    nodeWithBestPath.CostPath = int.MaxValue;

    while (reachables.Count > 0) {
      Node currentNode = ChoseNode(reachables);

      if ((currentNode.X == graph.Last().X) && (currentNode.Y == graph.Last().Y)) {
        // check_best_path_to_current_node
        if (nodeWithBestPath.CostPath > currentNode.CostPath)
          nodeWithBestPath = currentNode;
      }

      reachables.Remove(currentNode);
      explored.Add(currentNode);

      List<Node> newReachable = MyExept(
        graph.Find(fnd => {
          return (currentNode.X == fnd.X) &&
                 (currentNode.Y == fnd.Y);
        }).AdjacentNodes, explored);

      foreach (Node adjacent in newReachable) {

        reachables.Add(adjacent);

        int stepCost = Math.Abs(currentNode.Weight - adjacent.Weight) + 1;

        if (adjacent.IsDiagonal == true) {
          if (currentNode.DiagonalStep == true) {
            stepCost++;
          }
          adjacent.DiagonalStep = !currentNode.DiagonalStep;
        }

        adjacent.CostStep = stepCost;

        int updCostPath = currentNode.CostPath + stepCost;

        if (adjacent.CostPath == 0 || updCostPath < adjacent.CostPath) {
          adjacent.CostPath = updCostPath;
          adjacent.Previous = currentNode;
        }
      }
    }

    int height = map.GetLength(0);
    int width = map.GetLength(1);

    if (nodeWithBestPath.X != (width - 1) || nodeWithBestPath.Y != (height - 1)) {
      ExceptWriteToFile("Rover cannot find path.");
      throw new CannotStartMovement("Rover cannot find path.");
    }


    BuildPath(nodeWithBestPath);
  }

  private static List<Node> CreateGraph(string[,] arr) {
    // n - height -- Y
    // m - widht -- X

    List<Node> graph = new();

    int height = arr.GetLength(0);
    int width = arr.GetLength(1);

    try {

      for (int n = 0; n < height; n++) {
        for (int m = 0; m < width; m++) {

          if (arr[n, m] == "X") {
            continue;
          }

          Node current = new(n, m, int.Parse(arr[n, m]));

          int up = n - 1;
          int right = m + 1;
          int bottom = n + 1;
          int left = m - 1;

          if (up >= 0 && arr[up, m] != "X") {
            Node adj = new Node(up, m, int.Parse(arr[up, m]));
            current.AdjacentNodes.Add(adj);
          }
          if (left >= 0 && arr[n, left] != "X") {
            Node adj = new Node(n, left, int.Parse(arr[n, left]));
            current.AdjacentNodes.Add(adj);
          }
          if (bottom < height && arr[bottom, m] != "X") {
            Node adj = new Node(bottom, m, int.Parse(arr[bottom, m]));
            current.AdjacentNodes.Add(adj);
          }
          if (right < width && arr[n, right] != "X") {
            Node adj = new Node(n, right, int.Parse(arr[n, right]));
            current.AdjacentNodes.Add(adj);
          }

          if (up >= 0 && right < width && arr[up, right] != "X") {
            Node adj_diagonal = new Node(up, right, int.Parse(arr[up, right]), true);
            current.AdjacentNodes.Add(adj_diagonal);
          }
          if (bottom < height && right < width && arr[bottom, right] != "X") {
            Node adj_diagoanl = new Node(bottom, right, int.Parse(arr[bottom, right]), true);
            current.AdjacentNodes.Add(adj_diagoanl);
          }
          if (bottom < height && left >= 0 && arr[bottom, left] != "X") {
            Node adj_diagonal = new Node(bottom, left, int.Parse(arr[bottom, left]), true);
            current.AdjacentNodes.Add(adj_diagonal);
          }
          if (up >= 0 && left >= 0 && arr[up, left] != "X") {
            Node adj_diagonal = new Node(up, left, int.Parse(arr[up, left]), true);
            current.AdjacentNodes.Add(adj_diagonal);
          }
          graph.Add(current);
        }
      }
    }
    catch (FormatException ex) {
      ExceptWriteToFile("Map contains a wrong-formatted square.");
      throw ex;
    }

    return graph;
  }

  private static void BuildPath(Node destination) {
    List<Node> roverPath = new();

    while (destination != default) {
      roverPath.Add(destination);
      destination = destination.Previous;
    }

    roverPath.Reverse();

    string outStr = default;
    int steps = -1;
    int fuel = roverPath.Last().CostPath;

    foreach (var move in roverPath) {
      if (move != roverPath.Last())
        outStr += "[" + move.Y + "][" + move.X + "]->";
      else
        outStr += "[" + move.Y + "][" + move.X + "]";

      steps++;
    }

    outStr += "\nsteps: " + steps + "\nfuel: " + fuel;

    string path = @".\path-plan.txt";
    using StreamWriter sw = File.CreateText(path);
    sw.Write(outStr);
  }

  private static Node ChoseNode(List<Node> reachable) {
    int minCost = int.MaxValue;
    Node optimal = default;

    foreach (var node in reachable) {
      if (minCost > node.CostPath) {
        minCost = node.CostPath;
        optimal = node;
      }
    }

    return optimal;
  }

  private static List<Node> MyExept(List<Node> thisList, List<Node> remove) {
    List<Node> resultList =
        thisList.Where(save => !remove.Any(rem => (rem.X == save.X) && (rem.Y == save.Y))).ToList();

    return resultList;
  }

  private static void ExceptWriteToFile(string except) {
    string path = @".\path-plan.txt";
    using StreamWriter sw = File.CreateText(path);
    sw.Write(except);
  }

}

public class Node {
  public Node(int y, int x, int weight) {
    this.Y = y;
    this.X = x;
    this.Weight = weight;
  }
  public Node(int y, int x, int weight, bool isDiagonal) {
    this.Y = y;
    this.X = x;
    this.Weight = weight;
    this.IsDiagonal = isDiagonal;
  }

  public int X = 0;
  public int Y = 0;
  public int Weight = 0;
  public int CostStep = 0;
  public int CostPath = 0;
  public bool DiagonalStep = false;
  public bool IsDiagonal = false;

  public Node Previous;
  public List<Node> AdjacentNodes = new();
}

public class CannotStartMovement : Exception {
  public CannotStartMovement(string message) : base(message) { }
}